# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import utils
import torch
# import wandb
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from model.learner import EmpiricalRiskMinimizer
from model.arnetmodel import ARNetModel
from torch.utils.data import DataLoader
from model.data_loader import HeadEyesDataset
from model.data_loader import HDF5DataLoader

import torchvision.transforms as transforms


if __name__ == "__main__":


    parameters = sys.argv

    _, epochs, batch_size, learning_rate = parameters

    epochs        = int(epochs)
    batch_size    = int(batch_size)
    learning_rate = float(learning_rate)

    # Wandb 
    # wandb.init(project="Gaze")
    # wandb.config.epochs = epochs
    # wandb.config.batch_size = batch_size
    # wandb.config.learning_rate = learning_rate

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    if torch.cuda.is_available():
        in_cluster  = True
        num_workers = 8
    else:
        in_cluster  = False
        num_workers = 0

    ##### Uncomment this lines in order to train with .h5

    # transform = transforms.Compose([
    #     transforms.ToPILImage(),
    #     transforms.ToTensor()]
    # ) 

    # data_train = HDF5DataLoader(path_to_h5_file_train, transform=transform)
    # data_test  = HDF5DataLoader(path_to_h5_file_validation, transform=transform)


    ### Comment this lines in order to train with .h5

    data_train = HeadEyesDataset(
        "datasets/train/left_eye.npy",
        "datasets/train/right_eye.npy",
        "datasets/train/head.npy",
        "datasets/train/gaze.npy",
        in_cluster=in_cluster
    )

    data_test = HeadEyesDataset(
        "datasets/validation/left_eye.npy",
        "datasets/validation/right_eye.npy",
        "datasets/validation/head.npy",
        "datasets/validation/gaze.npy",
        in_cluster=in_cluster
    )



    train_batch_view = DataLoader(data_train, batch_size=batch_size, shuffle=True)
    test_batch_view = DataLoader(data_test, batch_size=batch_size, shuffle=True)

    model = ARNetModel()

    if torch.cuda.device_count() > 1:
        print("Using {} GPUs".format(torch.cuda.device_count()))
        model = nn.DataParallel(model)

    model.to(device)

    loss = F.mse_loss

    optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()),lr = learning_rate, betas=(0.9,0.95))

    # Watch the gradients
    # wandb.watch(model)

    erm = EmpiricalRiskMinimizer(model, loss, optimizer, device)    

    erm.fit(epochs, train_batch_view, validate=True, test_loader=test_batch_view)

    erm.save("saved_runs")
