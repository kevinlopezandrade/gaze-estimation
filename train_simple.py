import sys
import utils
import torch
import wandb
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from model.learner import EmpiricalRiskMinimizer
from model.simplemodel import SimpleModel
from torch.utils.data import DataLoader
from model.data_loader import JointDataset


if __name__ == "__main__":


    parameters = sys.argv

    _, epochs, batch_size, learning_rate = parameters

    epochs        = int(epochs)
    batch_size    = int(batch_size)
    learning_rate = float(learning_rate)

    # Wandb 
    wandb.init(project="Gaze")
    wandb.config.epochs = epochs
    wandb.config.batch_size = batch_size
    wandb.config.learning_rate = learning_rate

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    if torch.cuda.is_available():
        in_cluster  = True
        num_workers = 0
    else:
        in_cluster  = False
        num_workers = 0


    data = JointDataset(
        "datasets/face_dataset.npy",
        "datasets/eye_region_dataset.npy",
        "datasets/left_eye_dataset.npy",
        "datasets/right_eye_dataset.npy",
        "datasets/face_landmarks_dataset.npy",
        "datasets/head_dataset.npy",
        "datasets/gaze_dataset.npy",
        in_cluster=in_cluster
    )

    train_batch_view = DataLoader(data, batch_size=batch_size, shuffle=True)

    model = SimpleModel()

    if torch.cuda.device_count() > 1:
        print("Using {} GPUs".format(torch.cuda.device_count()))
        model = nn.DataParallel(model)

    model.to(device)

    loss = F.mse_loss

    optimizer = optim.SGD(model.parameters(),lr = learning_rate)

    # Watch the gradients
    wandb.watch(model)

    erm = EmpiricalRiskMinimizer(model, loss, optimizer, device)    

    print("Start training")
    erm.fit(epochs, train_batch_view)
    erm.save("saved_runs")
