# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import gzip
import time
# import wandb
import torch
import torch.nn as nn
import model.metrics as metrics

from os import path
from time import strftime
from torch.utils.data import DataLoader
from datetime import datetime, timedelta


def train(model, loss_fn, optimizer, batch_view, device):
    model.train()

    ang_error     = 0.0
    expected_risk = 0.0

    for X, Y in batch_view:
        # Set the gradients to zero
        optimizer.zero_grad()

        M = Y.shape[0]

        for key, value in X.items():
            X[key] = value.to(device, dtype=torch.float)

        Y = Y.to(device, dtype=torch.float)

        Y_ = model(X)

        loss = loss_fn(Y_, Y)

        # Backpropagation
        loss.backward()
        
        # Update weights
        optimizer.step()

        aux = metrics.pytorch_angular_error_from_pitchyaw(Y_, Y)
        ang_error += aux.item() * M
        expected_risk += loss.item() * M

    return ang_error, expected_risk


def test(model, loss_fn, batch_view, device, callback=None):
    model.eval()

    expected_risk = 0.0
    ang_error     = 0.0

    with torch.no_grad():

        for X, Y in batch_view:

            for key, value in X.items():
                X[key] = value.to(device, dtype=torch.float)

            Y = Y.to(device, dtype=torch.float32)
        
            M = Y.shape[0]

            Y_ = model(X)

            loss = loss_fn(Y_, Y)

            aux = metrics.pytorch_angular_error_from_pitchyaw(Y_, Y)
            ang_error += (aux.item() * M)
            expected_risk += (loss.item() * M)

    if callback is not None:
        callback(X, Y, Y_)

    return ang_error, expected_risk


class EmpiricalRiskMinimizer:
    """Abstract class to be more agnostic at the time of
    testing new models"""

    def __init__(self, model, loss, optimizer, device):

        self.model     = model
        self.loss_fn   = loss
        self.optimizer = optimizer
        self.device    = device

        # None intializations
        self.hyperparameters = None
        self.risks = None

    def fit(self, epochs, data_loader, validate=False, test_loader=None, callback=None):

            model     = self.model
            loss_fn   = self.loss_fn
            optimizer = self.optimizer
            device    = self.device

            # Nasty hack to more clean code
            batch_size = data_loader.batch_size

            # Nasty hack to more clean code
            N = len(data_loader.dataset)
            if validate:
                N_test = len(test_loader.dataset)

            try:
                print("Training has started")
                for epoch in range(1, epochs+1):
                    start_time = time.perf_counter()
                    
                    angular_error, expected_risk = train(model, loss_fn, optimizer, data_loader, device)

                    angular_error = angular_error * 1/N
                    expected_risk = expected_risk * 1/N

                    end_time = time.perf_counter()

                    print("\nEpoch {:5} computed in {:4.3f} s".format(epoch, end_time - start_time))
                    print("Ang-error {:<}".format(angular_error))
                    print("Exp-risk {:<}".format(expected_risk))


                    # wandb.log({'Angular error': angular_error}, commit=False)
                    # wandb.log({'Expected Risk': expected_risk}, commit=False)

                    # wandb.run.summary["epoch_time"] = end_time - start_time

                    # Test also the empirical test error
                    if validate:
                        angular_test_error, expected_test_risk = test(model, loss_fn, test_loader, device, callback)

                        angular_test_error = angular_test_error * 1/N_test
                        expected_test_risk = expected_test_risk * 1/N_test

                        print("Ang-Test-Error {:<}".format(angular_test_error))
                        print("Exp-Test-Risk {:<}".format(expected_test_risk))

                        # wandb.log({'Angular Test Error': angular_test_error}, commit=False)
                        # wandb.log({'Expected Test Risk': expected_test_risk}, commit=False)

                    else:
                        # Set the metrics to none
                        angular_test_error = None
                        expected_test_risk = None


                    # wandb.log()


                # Once the training has finsihed save the hyperparameters
                hyperparameters = {
                        'epochs': epochs,
                        'last_epoch': epoch,
                        'batch_size': batch_size,
                        'optimizer': optimizer.state_dict()
                }

                self.hyperparameters = hyperparameters

                risks = {
                        'angular_error': angular_error,
                        'angular_test_error': angular_test_error, 
                        'expected_risk': expected_risk,
                        'expected_test_risk': expected_test_risk 
                }

                self.risks = risks

            # Handle excpetion and save the las computed values
            except KeyboardInterrupt:
                print("Training paused, saving last epoch")
                hyperparameters = {
                        'epochs': epochs,
                        'last_epoch': epoch - 1,
                        'batch_size': batch_size,
                        'optimizer': optimizer.state_dict()
                }

                self.hyperparameters = hyperparameters

                risks = {
                        'angular_error': angular_error,
                        'angular_test_error': angular_test_error, 
                        'expected_risk': expected_risk,
                        'expected_test_risk': expected_test_risk 
                }

                self.risks = risks

    def save(self, directory):

        if self.hyperparameters is None:
            raise ValueError("Nothing to save, the model was not trained")

        model = self.model
        hyperparameters = self.hyperparameters
        risks = self.risks

        if model.__class__.__name__ == "DataParallel":
            model = model.module

        time_format = '%d-%m-%Y-%H:%M:%S'
        date = datetime.utcnow() + timedelta(hours=2)

        name = "{}-{}.pt".format(model.__class__.__name__, date.strftime(time_format))

        file_path = path.join(directory, name)

        to_save = {
                'model': model.state_dict(),
                'hyperparameters': hyperparameters,
                'risks': risks
        }

        torch.save(to_save, file_path) 

        print("Model saved as {}".format(name))

    def arg_where_bad(self, dset, beta=6):
        M = len(dset)
        model = self.model
        device = self.device

        model.eval()

        indexes_bad = []

        for index in range(M):
            value = dset[index]
            X, Y = value
            for key, val in X.items():
                val = torch.unsqueeze(val, dim=0)
                X[key] = val.to(device, dtype=torch.float)

            Y = torch.unsqueeze(Y, dim=0)
            Y = Y.to(device, dtype=torch.float32)

            Y_ = model(X)
            error = metrics.pytorch_angular_error_from_pitchyaw(Y, Y_)

            if(error > beta):
                print(index)
                indexes_bad.append(index)

        return indexes_bad

class Submission:
    def __init__(self, model, test_data, device):

        self.model     = model
        self.test_data = test_data
        self.device    = device

    def create_submission(self, directory):

        device    = self.device
        model     = self.model
        test_data = self.test_data

        if model.__class__.__name__ == "DataParallel":
            model = model.module

        time_format = '%d-%m-%Y-%H:%M:%S'
        date = datetime.utcnow() + timedelta(hours=2)

        name = "Submission-{}-{}.txt.gz".format(model.__class__.__name__, date.strftime(time_format))
        file = path.join(directory, name)

        data_view = DataLoader(test_data, batch_size=1)

        model.eval()

        aux = ""

        for x, _ in data_view:

            for key, value in x.items():
                x[key] = value.to(device, dtype=torch.float)

            y = model(x)[0]

            single_prediction = "{} {}\n".format(y[0], y[1])

            aux += single_prediction

        predictions = bytes(aux, encoding="UTF-8")

        with gzip.open(file, 'wb') as f:
            f.write(predictions)

        print("Submission saved as {}".format(name))
