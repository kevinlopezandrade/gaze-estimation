# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import torch.nn as nn
import torch
import torch.nn.functional as F
import torchvision
from torchvision import datasets, models, transforms

def init_weights(m):
    if type(m) == nn.Linear:
        torch.nn.init.xavier_uniform(m.weight)
        m.bias.data.fill_(0.01)

class ARNetModel(nn.Module):
    def __init__(self):
        super().__init__()
        self.vgg16 = models.vgg11_bn(pretrained=True)
        #for param in self.vgg16.parameters():
        #    param.requires_grad = False
        self.vgg16.classifier = nn.Sequential(
                     nn.Linear(25088, 1024),
                     nn.BatchNorm1d(1024),
                     nn.ReLU(True),
                     nn.Dropout(),
                     )
        self.fcj = nn.Sequential(
            nn.Linear(2048, 1024),
            nn.ReLU(True),
            nn.Dropout(),
        )
        self.fcj.apply(init_weights)
        self.fc2 = nn.Linear(3072, 512)
        self.fc2.apply(init_weights)
        self.fc3 = nn.Sequential(
                     nn.BatchNorm1d(514),
                     nn.ReLU(True),
                     nn.Linear(514, 256),
                     nn.ReLU(True),
                     nn.Linear(256, 2),
                     ) 
        self.fc3.apply(init_weights)
    def forward(self, X):
        left_eye = X['left-eye']
        right_eye = X['right-eye']
        head = X['head']

        xr = self.vgg16(right_eye)
        xl = self.vgg16(left_eye)
        joint = torch.cat((xl, xr), 1)
        joint = self.fcj(joint)
        x = torch.cat((xl, xr, joint), 1)
        x = self.fc2(x)
        x = torch.cat((x,head), 1)
        x = self.fc3(x)

        return x
