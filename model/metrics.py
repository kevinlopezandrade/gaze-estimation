import cv2 as cv
import numpy as np
import torch

def pitchyaw_to_vector(pitchyaws):
    r"""Convert given yaw (:math:`\theta`) and pitch (:math:`\phi`) angles to unit gaze vectors.

    Args:
        pitchyaws (:obj:`numpy.array`): yaw and pitch angles :math:`(n\times 2)` in radians.

    Returns:
        :obj:`numpy.array` of shape :math:`(n\times 3)` with 3D vectors per row.
    """
    n = pitchyaws.shape[0]
    sin = np.sin(pitchyaws)
    cos = np.cos(pitchyaws)
    out = np.empty((n, 3))
    out[:, 0] = np.multiply(cos[:, 0], sin[:, 1])
    out[:, 1] = sin[:, 0]
    out[:, 2] = np.multiply(cos[:, 0], cos[:, 1])
    return out


def vector_to_pitchyaw(vectors):
    r"""Convert given gaze vectors to yaw (:math:`\theta`) and pitch (:math:`\phi`) angles.

    Args:
        vectors (:obj:`numpy.array`): gaze vectors in 3D :math:`(n\times 3)`.

    Returns:
        :obj:`numpy.array` of shape :math:`(n\times 2)` with values in radians.
    """
    n = vectors.shape[0]
    out = np.empty((n, 2))
    vectors = np.divide(vectors, np.linalg.norm(vectors, axis=1).reshape(n, 1))
    out[:, 0] = np.arcsin(vectors[:, 1])  # theta
    out[:, 1] = np.arctan2(vectors[:, 0], vectors[:, 2])  # phi
    return out

radians_to_degrees = 180.0 / np.pi

def angular_error(a, b):
    """Calculate angular error (via cosine similarity)."""
    a = pitchyaw_to_vector(a) if a.shape[1] == 2 else a
    b = pitchyaw_to_vector(b) if b.shape[1] == 2 else b

    ab = np.sum(np.multiply(a, b), axis=1)
    a_norm = np.linalg.norm(a, axis=1)
    b_norm = np.linalg.norm(b, axis=1)

    # Avoid zero-values (to avoid NaNs)
    a_norm = np.clip(a_norm, a_min=1e-7, a_max=None)
    b_norm = np.clip(b_norm, a_min=1e-7, a_max=None)

    similarity = np.divide(ab, np.multiply(a_norm, b_norm))

    return np.arccos(similarity) * radians_to_degrees


def mean_angular_error(a, b):
    """Calculate mean angular error (via cosine similarity)."""
    return np.mean(angular_error(a, b))


def pytorch_angular_error_from_pitchyaw(y_true, y_pred):
    def angles_to_unit_vectors(y):
        sin = torch.sin(y)
        cos = torch.cos(y)
        return torch.stack([
            torch.mul(cos[:, 0], sin[:, 1]),
            sin[:, 0],
            torch.mul(cos[:, 0], cos[:, 1]),
        ], dim=1)

    v_true = angles_to_unit_vectors(y_true)
    v_pred = angles_to_unit_vectors(y_pred)

    return pytorch_angular_error_from_vector(v_true, v_pred)

def pytorch_angular_error_from_vector(v_true, v_pred):
        v_true_norm = torch.sqrt(torch.sum(torch.pow(v_true,2), dim=1))
        v_pred_norm = torch.sqrt(torch.sum(torch.pow(v_pred,2), dim=1))

        sim = torch.div(torch.sum(torch.mul(v_true, v_pred), dim=1),
                     torch.mul(v_true_norm, v_pred_norm))

        # Floating point precision can cause sim values to be slightly outside of
        # [-1, 1] so we clip values
        sim = torch.clamp(sim, -1.0 + 1e-6, 1.0 - 1e-6)

        ang = radians_to_degrees * torch.acos(sim)

        return torch.mean(ang)
