import h5py
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data
from collections import OrderedDict
import torchvision.transforms as transforms
import numpy as np

remove_bad = False
badtxt = "bad5.out"


class HeadEyesLandmarksDataset(data.Dataset):

    def __init__(self, path_left_eye, path_right_eye, path_face_landmarks, path_head,
                 path_gaze, in_cluster=False):

        if in_cluster:
            in_ram = True
            print("Data will be loaded in ram, may take a while")
        else:
            in_ram = False

        self.left_eye = LeftEyeDataset(path_left_eye, in_ram=in_ram)
        self.right_eye = RightEyeDataset(path_right_eye, in_ram=in_ram)
        self.face_landmarks = FaceLandmarksDataset(path_face_landmarks, in_ram=in_ram)
        self.head = HeadDataset(path_head, in_ram=in_ram)
        self.gaze = GazeDataset(path_gaze, in_ram=in_ram)

        print("Data Loaded")

    def __len__(self):
        return len(self.head)

    def __getitem__(self, index):

        left_eye = self.left_eye.__getitem__(index)
        right_eye = self.right_eye.__getitem__(index)
        face_landmarks = self.face_landmarks.__getitem__(index)
        head = self.head.__getitem__(index)
        gaze = self.gaze.__getitem__(index)

        x = {}
        x['left-eye'] = left_eye
        x['right-eye'] = right_eye
        x['face-landmarks'] = face_landmarks
        x['head'] = head

        y = gaze

        return x, y

class FaceHeadEyesDataset(data.Dataset):

    def __init__(self, path_face, path_left_eye, path_right_eye, path_head, path_gaze, in_cluster=False):

        if in_cluster:
            in_ram = True
            print("Data will be loaded in ram, may take a while")
        else:
            in_ram = False

        self.face = FaceDataset(path_face, in_ram=in_ram)
        self.left_eye = LeftEyeDataset(path_left_eye, in_ram=in_ram)
        self.right_eye = RightEyeDataset(path_right_eye, in_ram=in_ram)
        self.head = HeadDataset(path_head, in_ram=in_ram)
        self.gaze = GazeDataset(path_gaze, in_ram=in_ram)

        print("Data Loaded")

    def __len__(self):
        return len(self.head)

    def __getitem__(self, index):

        face = self.face.__getitem__(index)
        left_eye = self.left_eye.__getitem__(index)
        right_eye = self.right_eye.__getitem__(index)
        head = self.head.__getitem__(index)
        gaze = self.gaze.__getitem__(index)

        x = {}
        x['face'] = face
        x['left-eye'] = left_eye
        x['right-eye'] = right_eye
        x['head'] = head

        y = gaze

        return x, y

class OnlyFaceDataset(data.Dataset):

    def __init__(self, path_face, path_gaze, in_cluster=False):

        if in_cluster:
            in_ram = True
            print("Data will be loaded in ram, may take a while")
        else:
            in_ram = False

        self.face = FaceDataset(path_face, in_ram=in_ram)
        self.gaze = GazeDataset(path_gaze, in_ram=in_ram)

        print("Data Loaded")

    def __len__(self):
        return len(self.face)

    def __getitem__(self, index):

        face = self.face.__getitem__(index)
        gaze = self.gaze.__getitem__(index)

        x = {}
        x['face'] = face

        y = gaze

        return x, y


class HeadEyesDataset(data.Dataset):

    def __init__(self, path_left_eye, path_right_eye, path_head, path_gaze, in_cluster=False):


        if in_cluster:
            in_ram = True
            print("Data will be loaded in ram, may take a while")
        else:
            in_ram = False

        self.left_eye       = LeftEyeDataset(path_left_eye,             in_ram=in_ram)
        self.right_eye      = RightEyeDataset(path_right_eye,           in_ram=in_ram)
        self.head           = HeadDataset(path_head,                    in_ram=in_ram)
        self.gaze           = GazeDataset(path_gaze,                    in_ram=in_ram)

        print("Data Loaded")

    def __len__(self):
        return len(self.head)
    
    def __getitem__(self, index):


        left_eye       = self.left_eye.__getitem__(index)
        right_eye      = self.right_eye.__getitem__(index)
        head           = self.head.__getitem__(index)
        gaze           = self.gaze.__getitem__(index)

        x = {}
        x['left-eye'] = left_eye
        x['right-eye'] = right_eye
        x['head'] = head

        y = gaze

        return x, y

class FaceEyesDataset(data.Dataset):

    def __init__(self, path_face, path_left_eye, path_right_eye, path_gaze, in_cluster=False):


        if in_cluster:
            in_ram = True
            print("Data will be loaded in ram, may take a while")
        else:
            in_ram = False

        self.face           = FaceDataset(path_face,                    in_ram=in_ram)
        self.left_eye       = LeftEyeDataset(path_left_eye,             in_ram=in_ram)
        self.right_eye      = RightEyeDataset(path_right_eye,           in_ram=in_ram)
        self.gaze           = GazeDataset(path_gaze,                    in_ram=in_ram)

        print("Data Loaded")

    def __len__(self):
        return len(self.face)
    
    def __getitem__(self, index):

        face           = self.face.__getitem__(index)
        left_eye       = self.left_eye.__getitem__(index)
        right_eye      = self.right_eye.__getitem__(index)
        gaze           = self.gaze.__getitem__(index)

        x = {}
        x['face'] = face
        x['left-eye'] = left_eye
        x['right-eye'] = right_eye

        y = gaze

        return x, y

class JointDataset(data.Dataset):

    def __init__(self, path_face, path_eye_region, path_left_eye, path_right_eye, path_face_landmarks, path_head, path_gaze, in_cluster=False):


        if in_cluster:
            in_ram = True
            print("Data will be loaded in ram, may take a while")
        else:
            in_ram = False

        self.face           = FaceDataset(path_face,                    in_ram=in_ram)
        self.eye_region     = EyeRegionDataset(path_eye_region,         in_ram=in_ram)
        self.left_eye       = LeftEyeDataset(path_left_eye,             in_ram=in_ram)
        self.right_eye      = RightEyeDataset(path_right_eye,           in_ram=in_ram)
        self.face_landmarks = FaceLandmarksDataset(path_face_landmarks, in_ram=in_ram)
        self.head           = HeadDataset(path_head,                    in_ram=in_ram)
        self.gaze           = GazeDataset(path_gaze,                    in_ram=in_ram)

        print("Data Loaded")

    def __len__(self):
        return len(self.face)
    
    def __getitem__(self, index):

        face           = self.face.__getitem__(index)
        eye_region     = self.eye_region.__getitem__(index)
        left_eye       = self.left_eye.__getitem__(index)
        right_eye      = self.right_eye.__getitem__(index)
        face_landmarks = self.face_landmarks.__getitem__(index)
        head           = self.head.__getitem__(index)
        gaze           = self.gaze.__getitem__(index)

        x = {}
        x['face'] = face
        x['eye-region'] = eye_region
        x['left-eye'] = left_eye
        x['right-eye'] = right_eye
        x['face-landmarks'] = face_landmarks
        x['head'] = head

        y = gaze

        return x, y


class FaceDataset(data.Dataset):

    def __init__(self, path, in_ram=False, transform=None):

        self.transform = transform

        if in_ram:
            data = np.load(path)
            if remove_bad:
                bad = np.loadtxt(badtxt)
                data = np.delete(data, bad, axis=0)
            data = torch.from_numpy(data).float()
            data = data / 255.0

            self.data = data
        else:
            data = np.load(path, mmap_mode="r")
            data = torch.from_numpy(data).float()
            data = data / 255.0

        self.data = data

    def __len__(self):
        return len(self.data)


    def __getitem__(self, index):

        img = self.data[index]

        return img

class EyeRegionDataset(data.Dataset):

    def __init__(self, path, in_ram=False, transform=None):

        self.transform = transform

        if in_ram:
            data = np.load(path)
            if remove_bad:
                bad = np.loadtxt(badtxt)
                data = np.delete(data, bad, axis=0)
            data = torch.from_numpy(data).float()
            data = data / 255.0

            self.data = data
        else:
            data = np.load(path, mmap_mode="r")
            data = torch.from_numpy(data).float()
            data = data / 255.0

            self.data = data

    def __len__(self):
        return len(self.data)


    def __getitem__(self, index):

        img = self.data[index]

        return img


class LeftEyeDataset(data.Dataset):

    def __init__(self, path, in_ram=False, transform=None):

        self.transform = transform

        if in_ram:
            data = np.load(path)
            if remove_bad:
                bad = np.loadtxt(badtxt)
                data = np.delete(data, bad, axis=0)
            data = torch.from_numpy(data).float()
            data /= 255.0

            self.data = data
        else:
            data = np.load(path, mmap_mode="r")
            data = torch.from_numpy(data).float()
            data = data / 255.0

            self.data = data

    def __len__(self):
        return len(self.data)


    def __getitem__(self, index):

        img = self.data[index]

        return img

class RightEyeDataset(data.Dataset):

    def __init__(self, path, in_ram=False, transform=None):

        self.transform = transform

        if in_ram:
            data = np.load(path)
            if remove_bad:
                bad = np.loadtxt(badtxt)
                data = np.delete(data, bad, axis=0)
            data = torch.from_numpy(data).float()
            data = data / 255.0

            self.data = data
        else:
            data = np.load(path, mmap_mode="r")
            data = torch.from_numpy(data).float()
            data = data / 255.0

            self.data = data

    def __len__(self):
        return len(self.data)


    def __getitem__(self, index):

        img = self.data[index]

        return img

class FaceLandmarksDataset(data.Dataset):

    def __init__(self, path, in_ram=False, transform=None):

        self.transform = transform

        if in_ram:
            data = np.load(path)
            if remove_bad:
                bad = np.loadtxt(badtxt)
                data = np.delete(data, bad, axis=0)
            data = torch.from_numpy(data)
            data = data / 255.0

            self.data = data
        else:
            data = np.load(path, mmap_mode="r")
            data = torch.from_numpy(data)
            data = data / 255.0

            self.data = data

    def __len__(self):
        return len(self.data)


    def __getitem__(self, index):

        x = self.data[index]

        return x

class GazeDataset(data.Dataset):

    def __init__(self, path, in_ram=False, transform=None):

        self.transform = transform

        if in_ram:
            data = np.load(path)
            if remove_bad:
                bad = np.loadtxt(badtxt)
                data = np.delete(data, bad, axis=0)
            data = torch.from_numpy(data)

            self.data = data
        else:
            data = np.load(path, mmap_mode="r")
            data = torch.from_numpy(data)

            self.data = data

    def __len__(self):
        return len(self.data)


    def __getitem__(self, index):

        x = self.data[index]

        return x

class HeadDataset(data.Dataset):

    def __init__(self, path, in_ram=False, transform=None):

        self.transform = transform

        if in_ram:
            data = np.load(path)
            if remove_bad:
                bad = np.loadtxt(badtxt)
                data = np.delete(data, bad, axis=0)
            data = torch.from_numpy(data)

            self.data = data
        else:
            data = np.load(path, mmap_mode="r")
            data = torch.from_numpy(data)

            self.data = data

    def __len__(self):
        return len(self.data)


    def __getitem__(self, index):

        x = self.data[index]

        return x


class HDF5DataLoader(data.Dataset):

    ## Code adapated from the skeleton provided

    def __init__(self, hdf_path, entries_to_use=None, transform=None):

        self.transform       = transform
        # Dictionary that maps an integer index to a key string
        # since we want to acces the data by index
        self.map_index_to_sample = {}

        # Initialize everything
        hdf5 = h5py.File(hdf_path, 'r')

        users_ids = list(hdf5.keys())

        index_counter = 0
        for id_number in users_ids:

            # First iter return the number of elements in that feature dataset
            n = next(iter(hdf5[id_number].values())).shape[0]

            for i in range(n):
                self.map_index_to_sample[index_counter] = (id_number, i)
                index_counter += 1
        

        # Use all available input data if not specified
        if entries_to_use is None:
            entries_to_use = list(next(iter(hdf5.values())).keys())

        self.entries_to_use = entries_to_use

        self.hdf5 = hdf5

        self.data_indexes = self.map_index_to_sample


    def __len__(self):
        return len(self.data_indexes)
    
    def __getitem__(self, index):
        """Given one index returns as x the dictionary of features
           and as y the gaze position"""

        key, index = self.data_indexes[index]
        data = self.hdf5[key]
        x = {}

        for name in self.entries_to_use:
            if name != 'gaze':
                x[name] = data[name][index, :]

        y = data['gaze'][index,:]
        
        if self.transform:
            x['right-eye'] = self.transform(x['right-eye'])
            x['left-eye'] = self.transform(x['left-eye'])
            x['face'] = self.transform(x['face'])


        return x, y
