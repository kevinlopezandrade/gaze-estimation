#!/bin/bash
epochs='100'
batch_sizes='150 200'
lrs='0.001'
trainer="train_simple.py"

AUXE=100
AUXB=64
AUXL=0.001
bsub -J b${AUXB}lr${AUXL} -N -o b100lr0.001run -n 4 -W 4:00 -R "rusage[mem=8000, ngpus_excl_p=1]" python $trainer $AUXE $AUXB $AUXL 

for epoch in $epochs
do
	for batch_size in $batch_sizes
	do
		for lr in $lrs
		do
			bsub -w "ended(b${AUXB}lr${AUXL})" -J b${batch_size}lr${lr} -N -o b${batch_size}lr${lr}run -n 4 -W 4:00 -R "rusage[mem=8000, ngpus_excl_p=1]" python $trainer $epoch $batch_size $lr
			AUXB=$batch_size
			AUXL=$lr
		done
	done
done
