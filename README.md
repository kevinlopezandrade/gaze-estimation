# Gaze Estimation

This repo contains the implementaion of a **Deep Neural Network** designed
to perform Gaze Estimation using the **MPIIGaze** dataset. The model is
implemented in PyTorch.

A PDF document with more details of the model and the empirical results is also included.
