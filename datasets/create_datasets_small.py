# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import h5py
import numpy as np

"""Script to pass from H5 to .npy to train faster"""


def create_right_eye_dataset(hdf_path, samples_dict, path):
    hdf5 = h5py.File(hdf_path, 'r')
    N = 500

    right_eye_matrix = np.empty((N, 3, 60, 90), dtype=np.uint8)

    for i in range(N):
        print("Saving sample {}".format(i))
        user_id, index = samples_dict[i]
        batch_entries  = hdf5[user_id]['right-eye']
        sample = batch_entries[index]

        sample_permuted = np.transpose(sample, [2, 0, 1])

        right_eye_matrix[i] = sample_permuted


    print(right_eye_matrix)
    print(right_eye_matrix.shape)

    np.save(path, right_eye_matrix)

def create_left_eye_dataset(hdf_path, samples_dict, path):
    hdf5 = h5py.File(hdf_path, 'r')
    N = 500

    left_eye_matrix = np.empty((N, 3, 60, 90), dtype=np.uint8)

    for i in range(N):
        print("Saving sample {}".format(i))
        user_id, index = samples_dict[i]
        batch_entries  = hdf5[user_id]['left-eye']
        sample = batch_entries[index]


        sample_permuted = np.transpose(sample, [2, 0, 1])

        left_eye_matrix[i] = sample_permuted


    print(left_eye_matrix)
    print(left_eye_matrix.shape)

    np.save(path, left_eye_matrix)


def create_head_dataset(hdf_path, samples_dict, path):
    hdf5 = h5py.File(hdf_path, 'r')
    N = 500

    head_matrix = np.empty((N, 2), dtype=np.float32)

    for i in range(N):
        print("Saving sample {}".format(i))
        user_id, index = samples_dict[i]
        batch_entries  = hdf5[user_id]['head']
        sample = batch_entries[index]

        head_matrix[i] = sample

    print(head_matrix)
    print(head_matrix.shape)

    np.save(path, head_matrix)


def create_gaze_dataset(hdf_path, samples_dict, path):
    hdf5 = h5py.File(hdf_path, 'r')
    N = 500

    gaze_matrix = np.empty((N, 2), dtype=np.float32)

    for i in range(N):
        print("Saving sample {}".format(i))
        user_id, index = samples_dict[i]
        batch_entries  = hdf5[user_id]['gaze']
        sample = batch_entries[index]

        gaze_matrix[i] = sample

    print(gaze_matrix)
    print(gaze_matrix.shape)

    np.save(path, gaze_matrix)

def create_face_landmarks_dataset(hdf_path, samples_dict, path):
    hdf5 = h5py.File(hdf_path, 'r')
    N = 500

    face_landmarks_matrix = np.empty((N, 33, 2), dtype=np.float32)

    for i in range(N):
        print("Saving sample {}".format(i))
        user_id, index = samples_dict[i]
        batch_entries  = hdf5[user_id]['face-landmarks']
        sample = batch_entries[index]

        face_landmarks_matrix[i] = sample
    
    print(face_landmarks_matrix)
    print(face_landmarks_matrix.shape)

    np.save(path, face_landmarks_matrix)

def create_face_dataset(hdf_path, samples_dict, path):
    hdf5 = h5py.File(hdf_path, 'r')
    N = 500

    face_matrix = np.empty((N, 3, 224, 224), dtype=np.uint8)

    for i in range(N):
        print("Saving sample {}".format(i))
        user_id, index = samples_dict[i]
        batch_entries  = hdf5[user_id]['face']
        sample = batch_entries[index]

        sample_permuted = np.transpose(sample, [2, 0, 1])
        face_matrix[i] = sample_permuted
    
    print(face_matrix)
    print(face_matrix.shape)

    np.save(path, face_matrix)

def create_eye_region_dataset(hdf_path, samples_dict, path):
    hdf5 = h5py.File(hdf_path, 'r')
    N = 500

    eye_region_matrix = np.empty((N, 3, 60, 224), dtype=np.uint8)

    for i in range(N):
        print("Saving sample {}".format(i))
        user_id, index = samples_dict[i]
        batch_entries  = hdf5[user_id]['eye-region']
        sample = batch_entries[index]

        sample_permuted = np.transpose(sample, [2, 0, 1])
        eye_region_matrix[i] = sample_permuted
    
    print(eye_region_matrix)
    print(eye_region_matrix.shape)

    np.save(path, eye_region_matrix)


def get_samples_dict(hdf_path):
    map_index_to_sample = {}

    # Initialize everything
    hdf5 = h5py.File(hdf_path, 'r')

    users_ids = list(hdf5.keys())

    index_counter = 0
    for id_number in users_ids:

        # First iter return the number of elements in that feature dataset
        n = next(iter(hdf5[id_number].values())).shape[0]


        for i in range(n):
            map_index_to_sample[index_counter] = (id_number, i)
            index_counter += 1

    return map_index_to_sample



if __name__ == '__main__':

    #============= TRAIN ============#

    train_samples_dict = get_samples_dict("./train/mp19_train.h5")

    create_eye_region_dataset("./train/mp19_train.h5", train_samples_dict, "./train/eye_region.npy")
    create_face_dataset("./train/mp19_train.h5", train_samples_dict, "./train/face.npy")
    create_face_landmarks_dataset("./train/mp19_train.h5", train_samples_dict, "./train/face_landmarks.npy")
    create_head_dataset("./train/mp19_train.h5", train_samples_dict, "./train/head.npy")
    create_left_eye_dataset("./train/mp19_train.h5", train_samples_dict, "./train/left_eye.npy")
    create_right_eye_dataset("./train/mp19_train.h5", train_samples_dict, "./train/right_eye.npy")
    create_gaze_dataset("./train/mp19_train.h5", train_samples_dict, "./train/gaze.npy")

    #============= VALIDATION ============#

    validation_samples_dict = get_samples_dict("./validation/mp19_validation.h5")

    create_eye_region_dataset("./validation/mp19_validation.h5", validation_samples_dict, "./validation/eye_region.npy")
    create_face_dataset("./validation/mp19_validation.h5", validation_samples_dict, "./validation/face.npy")
    create_face_landmarks_dataset("./validation/mp19_validation.h5", validation_samples_dict, "./validation/face_landmarks.npy")
    create_head_dataset("./validation/mp19_validation.h5", validation_samples_dict, "./validation/head.npy")
    create_left_eye_dataset("./validation/mp19_validation.h5", validation_samples_dict, "./validation/left_eye.npy")
    create_right_eye_dataset("./validation/mp19_validation.h5", validation_samples_dict, "./validation/right_eye.npy")
    create_gaze_dataset("./validation/mp19_validation.h5", validation_samples_dict, "./validation/gaze.npy")


    #============= TEST ============#

    test_samples_dict = get_samples_dict("./test/mp19_test_students.h5")

    create_eye_region_dataset("./test/mp19_test_students.h5", test_samples_dict, "./test/eye_region.npy")
    create_face_dataset("./test/mp19_test_students.h5", test_samples_dict, "./test/face.npy")
    create_face_landmarks_dataset("./test/mp19_test_students.h5", test_samples_dict, "./test/face_landmarks.npy")
    create_head_dataset("./test/mp19_test_students.h5", test_samples_dict, "./test/head.npy")
    create_left_eye_dataset("./test/mp19_test_students.h5", test_samples_dict, "./test/left_eye.npy")
    create_right_eye_dataset("./test/mp19_test_students.h5", test_samples_dict, "./test/right_eye.npy")
