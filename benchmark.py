# Script to test the performance of raw h5 or 
# numpy file with mmap_mode="r"

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import torch
import h5py
import torch.nn as nn
import time
import torchvision.transforms as transforms

from torch.utils.data import DataLoader
from model.data_loader import JointDataset
from model.data_loader import HDF5DataLoader



def epoch(batch_view, device):

    for i, batch in enumerate(batch_view):
        X, Y           = batch

        face           = X['face'].to(device,           dtype=torch.float)
        eye_region     = X['eye-region'].to(device,     dtype=torch.float)
        left_eye       = X['left-eye'].to(device,       dtype=torch.float)
        right_eye      = X['right-eye'].to(device,      dtype=torch.float)
        face_landmarks = X['face-landmarks'].to(device, dtype=torch.float)
        head           = X['head'].to(device,           dtype=torch.float)
        
        Y = Y.to(device, dtype=torch.float)

        print("Batch {} sent to device".format(i))




if __name__ == "__main__":

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    print("Device {}".format(device))

    if torch.cuda.is_available():
        in_cluster = True
    else:
        in_cluster = False

    data = JointDataset(
        "datasets/face_dataset.npy",
        "datasets/eye_region_dataset.npy",
        "datasets/left_eye_dataset.npy",
        "datasets/right_eye_dataset.npy",
        "datasets/face_landmarks_dataset.npy",
        "datasets/head_dataset.npy",
        "datasets/gaze_dataset.npy",
        in_cluster=in_cluster
    )

    batch_view = DataLoader(data, batch_size=100)
    start = time.perf_counter()
    average_time = 0.0

    for i in range(10):
        start = time.perf_counter()
        epoch(batch_view, device)
        end = time.perf_counter()
        average_time += (end - start)

    print(average_time/10)
