# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#!/bin/bash
epochs='100'
batch_sizes='150 200'
lrs='0.001'
trainer="train_simple.py"

for epoch in $epochs
do
	for batch_size in $batch_sizes
	do
		for lr in $lrs
		do
			python $trainer $epoch $batch_size $lr
		done
	done
done
