# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import utils
import torch
import wandb
import numpy as np
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from model.learner import Submission
from model.vggfacemodel import VGGFaceModel
from torch.utils.data import DataLoader
from model.data_loader import FaceHeadEyesDataset


if __name__ == "__main__":

    _, path_weights = sys.argv

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

    if torch.cuda.is_available():
        in_cluster  = True
        num_workers = 8
    else:
        in_cluster  = False
        num_workers = 0

    data_test = FaceHeadEyesDataset(
        "datasets/test/face.npy",
        "datasets/test/left_eye.npy",
        "datasets/test/right_eye.npy",
        "datasets/test/head.npy",
        "datasets/train/gaze.npy", # Dumb dataset to not change the api, obviusly not used to evaluate the model
        in_cluster=in_cluster
    )

    model = VGGFaceModel()
    state = utils.load_saved_run(path_weights)
    model = utils.transfer_saved_model(model, state['model'])

    if torch.cuda.device_count() > 1:
        print("Using {} GPUs".format(torch.cuda.device_count()))
        model = nn.DataParallel(model)

    model.to(device)

    loss = F.mse_loss

    optimizer = optim.Adam(filter(lambda p: p.requires_grad, model.parameters()),lr = 0.001, betas=(0.9,0.95))

    s = Submission(model, data_test, device)
    s.create_submission("submissions/")
