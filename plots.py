# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import torch
import visdom
import numpy as np


class VisdomWriter(visdom.Visdom):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.plots = {}

    def add_line_plot(self, tag, opts=None):

        if opts is None:
            opts = {}
            opts['title'] = tag

        win = self.line(torch.tensor([np.nan]), opts=opts)

        self.plots[tag] = win

        return self

    def add_img_plot(self, tag, opts=None, H=300, W=300):

        if opts is None:
            opts = {}
            opts['caption'] = tag
        
        blank_image = torch.ones(1, H, W).float()
        win = self.image(blank_image, opts=opts)

        self.plots[tag] = win

        return self


    def add_n_lines_plot(self, tag, num_lines, opts=None): 

        blank_points = torch.empty(1,num_lines)
        blank_points.fill_(np.nan)

        if opts is None:
            opts = {}
            opts['title'] = tag
        else:
            if 'title' not in opts:
                opts['title'] = tag


        win = self.line(blank_points, opts=opts)

        self.plots[tag] = win

        return self


    def update_img_plot(self, tag, img):

        win = self.plots[tag]
        self.image(img, win=win)

        return self


    def append_to_line(self, tag, y, x):

        Y = torch.tensor([y])
        X = torch.tensor([x])

        win = self.plots[tag]

        self.line(win=win, Y=Y, X=X, update='append')

        return self

    def append_to_n_lines(self, tag, *Y, x=None, opts=None):

        if len(Y) < 1:
            raise ValueError("Y should be a M array")
        
        if x is None:
            raise ValueError("x can't be none")

        win = self.plots[tag]

        Y = torch.tensor(Y).view(1,-1)
        X = torch.tensor(x).view(-1,)

        self.line(win=win, Y=Y, X=X, opts=opts, update='append')

        return self
