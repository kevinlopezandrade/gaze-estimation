# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import math
import time
import torch
import numpy as np
import cv2 as cv

from os import path
from time import strftime

def draw_gaze(image_in, eye_pos, pitchyaw, length=40.0, thickness=2, color=(0, 0, 255)):
    """Draw gaze angle on given image with a given eye positions."""
    image_out = image_in
    if len(image_out.shape) == 2 or image_out.shape[2] == 1:
        image_out = cv.cvtColor(image_out, cv.COLOR_GRAY2BGR)
    dx = -length * np.sin(pitchyaw[1])
    dy = -length * np.sin(pitchyaw[0])
    cv.arrowedLine(image_out, tuple(np.round(eye_pos).astype(np.int32)),
                   tuple(np.round([eye_pos[0] + dx, eye_pos[1] + dy]).astype(int)), color,
                   thickness, cv.LINE_AA, tipLength=0.2)
    return image_out


def transfer_saved_model(model_instance, state_dict, inference=False, strict=True):
    """Loads a saved model:
    inference: If we don't want to compute the gradients wrt to some parameter of the model
    by setting to true this argument we don't record the operations of the loaded model

    By default in pytorch all the parameters of the modules have the requires_grad set to true
    
    """

    model_instance.load_state_dict(state_dict, strict=strict)

    # By the default always return in evaluation mode
    model_instance.eval()

    if inference:
        # Change recursively the required grad for all the parameters
        for param in model_instance.parameters(recurse=True):
            param.requires_grad = False

    # Pass the model by default to the device, avoid errors at the time of loading the model in cpu
    if torch.cuda.is_available():
        pass

    return model_instance


def load_saved_run(model_path):

    saved_dict = None

    if torch.cuda.is_available():
        saved_dict = torch.load(model_path)
    else:
        saved_dict = torch.load(model_path, map_location='cpu')

    return saved_dict


def output_size_convolution_layer(H, W, kernel_size, stride, padding, dilation=1):
    """
    Returns the ouput size of a squared image after convoling
    with the specified parameters
    """

    numerator = H + ( 2 * padding) - (dilation * (kernel_size - 1)) - 1
    denominator = stride

    return math.floor((numerator/denominator) + 1)


def output_size_deconvolution_layer(H, W, kernel_size, stride, padding, output_padding, dilation=1):

    H_out = (H - 1) * stride - (2 * padding) + (dilation * (kernel_size - 1)) + output_padding + 1

    return H_out


class LogCallback:

    def __init__(self, logger):
        self.logger = logger

    def __call__(self, X, Y, X_):
        raise NotImplementedError
